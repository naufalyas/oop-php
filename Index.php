<?php
    echo "<h3>OOP PHP</h3>";
    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");
    $Animal= new Animal("Shaun");
    echo "Nama = " .$Animal->name ."<br>";
    echo "Legs = " .$Animal->legs ."<br>";
    echo "Cold_blooded = " .$Animal->cold_blooded ."<br><br>";

    $frog = new frog("Buduk");
    echo "Nama = " .$frog->name ."<br>";
    echo "Legs = " .$frog->legs ."<br>";
    echo "Cold_blooded = " .$frog->cold_blooded ."<br>";
    echo "Jump = " .$frog->jump ."<br><br>";

    $ape = new ape("Kera Sakti");
    echo "Nama = " .$ape->name ."<br>";
    echo "Legs = " .$ape->legs ."<br>";
    echo "Cold_blooded = " .$ape->cold_blooded ."<br>";
    echo "yell = " .$ape->yell ."<br>";

?>  
